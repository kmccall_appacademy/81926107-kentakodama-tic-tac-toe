
class Board

  attr_accessor :grid #this eventually allows access to board.grid method

  def initialize(grid='default')
    if grid == 'default'
      @grid = Array.new(3) { Array.new(3, nil) }
    else
      @grid = grid
    end
  end

  def place_mark(position, mark) #takes Array
      #if the position is empty then mark else raise error
      if empty?(position)
        y = position[0]
        x = position[1]
        @grid[y][x] = mark
      else
        raise 'Error'
      end
  end

  def empty?(position)

    y = position[0]
    x = position[1]
    return true if @grid[y][x] == nil

    false
  end

  def winner

    lines = get_lines(@grid)

    return :X if lines.include?([:X, :X, :X])
    return :O if lines.include?([:O, :O, :O])

    nil

  end


  def get_lines(matrix)
    lines = []
    matrix.each {|row| lines << row}
    matrix.transpose.each {|column| lines << column}
    lines << get_diagonal(matrix)
    lines << get_diagonal(matrix.transpose.reverse)
    lines
  end

  def get_diagonal(grid)
    line = []
    i = 0
    while i < grid.length
      line << grid[i][i]
      i += 1
    end
    line
  end


  def over?
    return winner if winner != nil
    return 'cats game' unless @grid.flatten.include?(nil)
    false
  end

end
