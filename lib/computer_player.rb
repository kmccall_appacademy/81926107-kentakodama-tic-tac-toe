
#
# In your ComputerPlayer class,
#
# display should store the board it's passed as an instance variable, so that get_move has access to it
# get_move should return a winning move if one is available, and otherwise move randomly.
# In your Game class, set the marks of the players you are passed. Include the following methods:
#
# current_player
# switch_players!
# play_turn, which handles the logic for a single turn
# play, which calls play_turn each time through a loop until the game is over
# Pro tips: * Both HumanPlayer and ComputerPlayer should have the same API; they should have the same set of public methods. This means they should be interchangeable. * Your Game class should be passed two player objects on instantiation; because both player classes have the same API, the game should not know nor care what kind of players it is given.

class ComputerPlayer

    attr_accessor :name, :mark, :get_move, :board

    def initialize(name)
      @name = name
      @mark = nil
    end
    #
    #still have to complete first gonna do the game class
    def display(game) #this game refers to an instance of the game class
      @board = game
    end

    # #"prints the board to the screen" do
    #   board.place_mark([0, 0], :X)
    #   human.display(board)
    #
    def get_move
      # this method must look into the grid of the board
      # if there are two in a row then complete third
      y = 0
      while y < @board.grid.length
        x = 0
        while x < @board.grid[y].length
          if @board.empty?([y, x])
            @board.grid[y][x] = self.mark
            if @board.winner != nil
              return [y, x]#this might be a problem
            else
              @board.grid[y][x] = nil
            end
          end
          x += 1
        end
        y += 1
      end
      #random move
      y = 0
      while y < @board.grid.length
        x = 0
        while x < @board.grid[y].length
          if @board.empty?([y, x])
            return [y, x]
          end
          x += 1
        end
        y += 1
      end

    end

end
